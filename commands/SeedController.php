<?php
    /**
     * Created by PhpStorm.
     * User: valentindaitkhe
     * Date: 20/05/2019
     * Time: 02:59
     */

    namespace app\commands;

    use app\models\Invoice;
    use app\models\User;
    use Faker\Factory;
    use Yii;
    use yii\base\Exception;
    use yii\console\Controller;
    use yii\console\ExitCode;

    class SeedController extends Controller
    {
        public function actionIndex($count = 10)
        {

            $faker = Factory::create();
            for ($i = 0; $i < $count; $i++) {
                $invoice = new Invoice();
                $invoice->currency = $faker->currencyCode;
                $invoice->due_date = $faker->dateTime->format('Y-m-d H:i:s');
                $invoice->amount = $faker->randomDigitNotNull;
                $invoice->bank_name = $faker->name;
                $invoice->iban = $faker->iban();
                $invoice->swift_code = $faker->swiftBicNumber;
                $invoice->image = $faker->imageUrl();
                $invoice->description = $faker->text(200);
                $invoice->save();
            }

            return ExitCode::OK;
        }

        public function actionUsers($count, $role = 'operator')
        {
            $auth = Yii::$app->authManager;
            $faker = Factory::create();
            $pwd = 'qwerty123';
            $roles = ['admin', 'operator'];
            $authRoles = $auth->getRoles();


            if (!in_array($role, $roles)) {
                echo 'Err : incorrect user role , chose from  [operator, admin]';
                return;
            }
            if (sizeof($authRoles) == 0) {
                array_walk($roles, function ($item) use ($auth) {
                    $role = $auth->createRole($item);
                    $auth->add($role);
                });
            }

            for ($i = 0; $i < $count; $i++) {
                $user = new User();
                $user->name = $faker->name;
                $user->email = $faker->email;
                try {
                    $user->password = Yii::$app->getSecurity()->generatePasswordHash($pwd);
                } catch (Exception $e) {
                    return $e->getMessage();
                }
                $user->setPassword($pwd);


                $user->save();

                $authorRole = $auth->getRole($role);

                try {
                    $auth->assign($authorRole, $user->getId());
                } catch (\Exception $e) {
                     echo $e->getMessage();
                }




            }
            return ExitCode::OK;
        }


    }
