<?php
    /**
     * Created by PhpStorm.
     * User: valentindaitkhe
     * Date: 23/05/2019
     * Time: 23:39
     */

    namespace app\commands;
    use app\models\Invoice;
    use app\models\Notifications;
    use app\models\User;
    use yii\base\Exception;
    use yii\console\Controller;
    use yii\console\ExitCode;


    class CronController extends Controller
    {
        public function actionIndex(){
            $time = new \DateTime('now');
            $today = $time->format('Y-m-d H:i:s');
            $expiredInvocies = Invoice::find()->where(['<=','due_date',$today])->all();

            foreach ($expiredInvocies as $invoice){
                $notice = new Notifications();
                $notice->message = 'Payment by invoice # '.$invoice['id'].' overdue.';
                $notice->invoice_id = $invoice['id'];
                $notice->status = 0;
                    $notice->save();
            }
            return ExitCode::OK;
        }

    }