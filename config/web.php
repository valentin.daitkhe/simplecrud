<?php

    use \yii\web\Request;

    $params = require __DIR__ . '/params.php';
    $db = require __DIR__ . '/db.php';

    $baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());
    $config = [
        'id' => 'basic',
        'name' => 'SimpleCrud',
        'basePath' => dirname(__DIR__),
        'bootstrap' => ['log'],
        'aliases' => [
            '@bower' => '@vendor/bower-asset',
            '@npm' => '@vendor/npm-asset',
        ],
        'components' => [
            'request' => [
                // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
                'cookieValidationKey' => 'pwDsxgnBYPYO5Qv5AN-gGejv4BoVg4EJ',
                'baseUrl' => $baseUrl
            ],
            'cache' => [
                'class' => 'yii\caching\FileCache',
            ],
            'user' => [
                'identityClass' => 'app\models\User',
                'enableAutoLogin' => false,
            ],
            'errorHandler' => [
                'errorAction' => 'site/error',
            ],
            'mailer' => [
                'class' => 'yii\swiftmailer\Mailer',
                // send all mails to a file by default. You have to set
                // 'useFileTransport' to false and configure a transport
                // for the mailer to send real emails.
                'useFileTransport' => true,
            ],
            'log' => [
                'traceLevel' => YII_DEBUG ? 3 : 0,
                'targets' => [
                    [
                        'class' => 'yii\log\FileTarget',
                        'levels' => ['error', 'warning'],
                    ],
                ],
            ],
            'db' => $db,

            'view' => [
                'class' => 'yii\web\View',
                'renderers' => [
                    'twig' => [
                        'class' => 'yii\twig\ViewRenderer',
                        'cachePath' => '@runtime/Twig/cache',
                        // Array of twig options:
                        'options' => [
                            'auto_reload' => true,
                        ],
                        'globals' => [
                            'html' => ['class' => '\yii\helpers\Html'],
                        ],
                        'uses' => ['yii\bootstrap'],
                    ],
                    // ...
                ],
            ],

           // ne ochen
            'authManager' => [
                'class' => 'yii\rbac\DbManager',
            ],
            'urlManager' => [
                'baseUrl' => $baseUrl,
                'enablePrettyUrl' => true,
                'showScriptName' => false,
                "rules" => [
                    "home" => "invoice/index",
                    "login" => "invoice/login",
                    "contact-us" => "site/contact",
                ]
            ],

        ],
        'params' => $params,
        'defaultRoute' => 'invoice/index',
    ];

    if (YII_ENV_DEV) {
        // configuration adjustments for 'dev' environment
//        $config['bootstrap'][] = 'debug';
////        $config['modules']['debug'] = [
////           // 'class' => 'yii\debug\Module',
////            // uncomment the following to add your IP if you are not connecting from localhost.
////            //'allowedIPs' => ['127.0.0.1', '::1'],
////        ];

        $config['bootstrap'][] = 'gii';
        $config['modules']['gii'] = [
            'class' => 'yii\gii\Module',
            // uncomment the following to add your IP if you are not connecting from localhost.
            //'allowedIPs' => ['127.0.0.1', '::1'],
        ];
    }

    return $config;
