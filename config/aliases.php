<?php
    /**
     * Created by PhpStorm.
     * User: valentindaitkhe
     * Date: 22/05/2019
     * Time: 02:10
     */
    use \yii\web\Request;
    $baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());
    Yii::setAlias('@storage', __DIR__ . '/../web/storage');
