<?php

    use app\models\Invoice;
    $time = new \DateTime('now');
    $today = $time->format('Y-m-d H:i:s');
    $schedule = new \omnilight\scheduling\Schedule();
    $data =  $expiredInvocies = Invoice::find()->where(['<=','due_date',$today])->all();
    $schedule->exec('cron')->everyMinute()->when(daily()->at('12:00')->when(function () use ($data){
        return sizeof($data) > 0;
    }));

