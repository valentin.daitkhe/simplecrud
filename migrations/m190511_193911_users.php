<?php

use yii\db\Migration;

/**
 * Class m190511_193911_users
 */
class m190511_193911_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
              $this->createTable('users', [
                  'id' => $this->primaryKey(),
                  'name' => $this->string()->null(),
                  'email'=> $this->string()->notNull()->unique(),
                  'password'=> $this->string()->notNull(),
              ]);
    }

}
