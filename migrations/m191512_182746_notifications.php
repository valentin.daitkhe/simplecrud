<?php

use yii\db\Migration;

/**
 * Class m191512_182746_notifications
 */
class m191512_182746_notifications extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('notifications', [
            ' id' => $this->primaryKey(),
             'invoice_id' => $this->integer()->unique(),
             'status'=> $this->boolean()->notNull(),
             'message'=>$this->text()->null()
        ]);
    }
}
