<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%invoce}}`.
 */
class m190519_233240_create_invoices_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('invoices', [
            'id' => $this->primaryKey(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at'=>$this->timestamp()->null(),
            'due_date' =>$this->dateTime()->notNull(),
            'amount' => $this->double()->notNull(),
            'currency'=> $this->string(5)->notNull(),
            'bank_name' =>$this->string(100)->notNull(),
            'iban' =>$this->string(40)->notNull(),
            'swift_code'=>$this->string(20)->notNull(),
            'image' => $this->string(500)->notNull(),
            'description'=>$this->text()->null()
        ]);
        $this->addForeignKey('invoice_id','notifications','invoice_id','invoices','id','CASCADE', 'CASCADE');
    }


}
