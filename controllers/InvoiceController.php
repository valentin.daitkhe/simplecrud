<?php

    namespace app\controllers;

    use app\models\LoginForm;
    use app\models\Notifications;
    use Yii;
    use app\models\Invoice;
    use yii\base\ErrorException;
    use yii\base\Exception;
    use yii\data\ActiveDataProvider;
    use yii\filters\AccessControl;
    use yii\helpers\FileHelper;
    use yii\helpers\Url;
    use yii\web\Controller;
    use yii\web\NotFoundHttpException;
    use yii\filters\VerbFilter;
    use yii\web\UploadedFile;


    class InvoiceController extends Controller
    {
        public function behaviors()
        {
            return [

                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
                'access' => [

                    'class' => AccessControl::className(),

                    'rules' => [
                        [
                            'actions' => ['login', 'error'],
                            'allow' => true,
                        ],
                        [
                            'actions' => ['logout', 'index' ,'call-back'],
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                        [
                            'allow' => true,
                            'actions' => ['index'],
                            'roles' =>  ['operator','admin'],
                        ],
                        [
                            'allow' => true,
                            'actions' => ['view'],
                            'roles' => ['operator','admin'],
                        ],
                        [
                            'allow' => true,
                            'actions' => ['create'],
                            'roles' => ['operator','admin'],
                        ],
                        [
                            'allow' => true,
                            'actions' => ['update'],
                            'roles' => ['admin'],
                        ],
                        [
                            'allow' => true,
                            'actions' => ['delete'],
                            'roles' => ['admin'],
                        ],
                    ],
                    'denyCallback' => function($rule, $action) {
                        return Yii::$app->response->redirect(['/invoice/login']);
                    },
                ],
            ];
        }



        public function actionIndex()
        {
            $dataProvider = new ActiveDataProvider([
                'query' => Invoice::find(),
            ]);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
            ]);
        }



        public function actionView($id)
        {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }


        public function actionCreate()
        {
            $model = new Invoice();
            if ($model->load(Yii::$app->request->post())) {
                $image = UploadedFile::getInstance($model, 'image');
                $dir = uniqid();
                $path = Yii::getAlias('@storage') . '/' . $dir;
                try {
                    FileHelper::createDirectory($path);
                } catch (Exception $e) {
                    Yii::getLogger()->log($e->getMessage(), 1);
                }

                $file = $dir . '/' . $image->getBaseName() . '.' . $image->getExtension();

                $image->saveAs(Yii::getAlias('@storage') . '/' . $file);

                $model->image = Url::to($img = Url::to('@web/storage/') . $file);

                $model->save(false);

                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('create', [
                'model' => $model,
            ]);
        }


        public function actionUpdate($id)
        {
            $model = $this->findModel($id);
            $imageID = explode('/', $model->image)[2];

            if ($model->load(Yii::$app->request->post())) {
                $image = UploadedFile::getInstance($model, 'image');
                $fileName = $imageID . '/' . $image->getBaseName() . '.' . $image->getExtension();
                $image->saveAs(Yii::getAlias('@storage') . '/' . $fileName);
                $model->image = Url::to($img = Url::to('@web/storage/') . $fileName);
                $model->save(false);
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        }


        public function actionDelete($id)
        {
          $model =   $this->findModel($id);
          $imageID = explode('/', $model->image)[2];
          $path  = $path = Yii::getAlias('@storage') . '/' . $imageID;
            try {
                FileHelper::removeDirectory($path);
            } catch (ErrorException $e) {

                Yii::getLogger()->log($e->getMessage(), 1);
            }
            return $this->redirect(['index']);
        }


        protected function findModel($id)
        {
            if (($model = Invoice::findOne($id)) !== null) {
                return $model;
            }

            throw new NotFoundHttpException('The requested page does not exist.');
        }
        public function actionLogout()
        {
            Yii::$app->user->logout();

            return $this->goHome();
        }



        public function actionLogin()
        {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }

            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                $this->checkAndLoadNotice();
                return $this->goBack();
            }

            $model->password = '';
            return $this->render('login', [
                'model' => $model,
            ]);
        }


        /// right way  for me is use  cronJobs or Db procedure
        public function loadNotice(){
            $time = new \DateTime('now');
            $today = $time->format('Y-m-d H:i:s');
            $expiredInvocies = Invoice::find()->where(['<=','due_date',$today])->all();

            foreach ($expiredInvocies as $invoice){
                $notice = new Notifications();
                $notice->message = 'Payment by invoice # '.$invoice['id'].' overdue.';
                $notice->invoice_id = $invoice['id'];
                $notice->status = 0;
                $notice->save();
            }
        }



    }
