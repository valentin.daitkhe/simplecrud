<?php

    namespace app\controllers;

    use Yii;
    use app\models\Notifications;
    use yii\data\ActiveDataProvider;
    use yii\filters\AccessControl;
    use yii\web\Controller;
    use yii\web\NotFoundHttpException;
    use yii\filters\VerbFilter;

    /**
     * NotificationController implements the CRUD actions for Notifications model.
     */
    class NotificationController extends Controller
    {

        public function behaviors()
        {
            return [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],

                ],
                'access' => [

                    'class' => AccessControl::className(),
                    'only' => ['view', 'index'],
                    'rules' => [

                        [
                            'allow' => true,
                            'actions' => ['view', 'index'],
                            'roles' => ['operator', 'admin'],
                        ]
                    ],

                ]
            ];
        }


        public function actionIndex()
        {
            $array = Notifications::find()->all();
            return $this->asJson($array);
        }


        protected function findModel($id)
        {
            if (($model = Notifications::findOne($id)) !== null) {
                return $model;
            }

            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
