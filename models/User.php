<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use yii\base\Security;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 *
 * @property Notifications[] $notifications
 */
class User extends ActiveRecord implements IdentityInterface
{
    protected  $sc;

    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $this->sc = new Security();
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'username',
            'email' => 'Email',
            'password' => 'Password',
        ];
    }


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
       return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
        
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['name' => $username]);
    }
    
    
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */

    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password,$this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        try {
            $this->password_hash =Yii::$app->getSecurity()->generatePasswordHash($password);
        } catch (Exception $e) {
            Yii::getLogger()->log($e->getMessage(),1);
        }
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        try {
            $this->auth_key = $this->sc->generateRandomKey();
        } catch (Exception $e) {
            Yii::getLogger()->log($e->getMessage(),1);
        }
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        try {
            $this->password_reset_token = $this->sc->generateRandomKey() . '_' . time();
        } catch (Exception $e) {
           Yii::getLogger()->log($e->getMessage(),1);
        }
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}
