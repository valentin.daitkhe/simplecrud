<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notifications".
 *
 * @property int $ id
 * @property int $invoice_id
 * @property int $status
 * @property string $message
 */
class Notifications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notifications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['invoice_id', 'status'], 'integer'],
            [['status'], 'required'],
            [['message'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            ' id' => 'Id',
            'invoice_id' => 'Invoice ID',
            'status' => 'Status',
            'message' => 'Message',
        ];
    }
    public function findByInvoiceID($id){
        return self::findOne(['invoice_id'=>$id]);
    }
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }
}
