<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class Invoice extends \yii\db\ActiveRecord
{



    public static function tableName()
    {
        return 'invoices';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => true,
                'value' => new Expression('NOW()'),
            ],

        ];
    }


    public function rules()
    {
        return [
            [['created_at', 'due_date', 'amount', 'currency', 'bank_name', 'iban', 'swift_code', 'image'], 'required'],
            [['created_at', 'updated_at', 'due_date'], 'safe'],
            [['amount'], 'number'],
            [['description'], 'string'],
            [['currency'], 'string', 'max' => 5],
            [['bank_name'], 'string', 'max' => 100],
            [['iban'], 'string', 'max' => 40],
            [['swift_code'], 'string', 'max' => 20],
            [['image'], 'string', 'max' => 500],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'due_date' => 'Due Date',
            'amount' => 'Amount',
            'currency' => 'Currency',
            'bank_name' => 'Bank Name',
            'iban' => 'Iban',
            'swift_code' => 'Swift Code',
            'image' => 'Image',
            'description' => 'Description',
        ];
    }


}
